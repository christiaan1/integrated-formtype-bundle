<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\FormTypeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class TinyMCEType
 * @package Integrated\Bundle\FormTypeBundle\Form\Type
 */
class TinyMCEType extends AbstractType
{
    /** @var array */
    private $contentStyles;

    /**
     * TinyMCEType constructor.
     * @param array $contentStyles
     */
    public function __construct(array $contentStyles)
    {
        $this->contentStyles = $contentStyles;
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars = array_merge($view->vars, ['content_styles' => $this->contentStyles]);
        $view->vars['mode'] = $options['mode'];
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'textarea';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'integrated_tinymce';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'mode' => "default",
        ));

        $resolver->setAllowedTypes(array(
            'mode' => 'string',
        ));
    }

}
